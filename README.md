# Baseline benchmark problem

## Description

Stochastic estimation of the trace of the inverse of Dirac-Wilson operators
accelerated with multigrid deflation. Most of the time goes into computing
1024~4048 eigenpairs from the coarsest operator,  applying the deflation, and
the solution of 12 times 512 RHSs with Dirac's operator. Next is a sketch of
the method:

```Matlab
P = generate_prolongator(A);
Ac = P'*A*P;  % coarse operator
[V,L] = eigs(gamma5 * Ac, numEigenpairs); % compute eigenpairs
t = [];
for i = 1:12*512
   x = generate_rhs(i);
   % Apply deflation
   x = x - A * (P * (gamma5 * (V * inv(L) * (P * (V' * x)))));
   % Solve RHS
   y = multigrid_solve(A, x);
   t(i) = x' * y;
end
```

## Figure of Merit

The average time for computing an eigenpair, for applying the deflation to a
vector, and for solving a RHS.

# Measurement on Edison

PRIMME computes the eigenpairs. QOPQDP's multigrid is used for the application of the deflation space
and the solution of the RHSs. Chrome reads the input files
and configures QOPQDP's multigrid accordingly. The code that estimates the
traces executes the loop in batches of 12, although  QOPQDP applies the prolongator and solves the RHSs vector by vector.

PRIMME is linked with MKL. Neither QOPQDP nor the dependent libraries are built with support for OpenMP. The
nodes are filled with MPI processes so that the total number pleases QOPQDP.

It is run three different heavy quark configurations with a number of nodes
approximately proportional to the lattice volume:

- 32^3 x 64 on 22 nodes, 512 MPI processes.
- 48^3 x 96 on 108 nodes, 1296 MPI processes.
- 64^3 x 128 on 256 nodes, 4096 MPI processes.

## Preliminary results

Timings in seconds:

| configuration |   eigs   | deflation | solve  |
|---------------|----------|-----------|--------|
| 32^3 x 64     |  0.4199  | 0.02653   | 0.1953 |
| 48^3 x 96     |  0.4150  | 0.08049   | 1.5577 |
| 64^3 x 128    |  0.7622  | 0.09163   | 2.7779 |


# Optimization Strategies

PRIMME has already ported to GPUs using MAGMA. The generation of the RHSs can
still be done on the CPU and copy the vectors to the GPU. The application of
deflation and the solution of RHSs can be done with QUDA. QUDA supports doing
both operations on several vectors at once.

